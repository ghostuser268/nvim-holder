call plug#begin('~/.vim/plugged')
  Plug 'junegunn/fzf', { 'do': 'yes \| ./install' }
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'preservim/nerdcommenter'
  Plug 'ghifarit53/tokyonight-vim'
  Plug 'tpope/vim-surround'
  Plug 'jiangmiao/auto-pairs'
  Plug 'ryanoasis/vim-devicons'
  Plug 'tpope/vim-obsession'
  Plug 'morhetz/gruvbox'
  Plug 'Yggdroot/indentLine'
  Plug 'ayu-theme/ayu-vim'
  Plug 'catppuccin/nvim', {'name': 'catppuccin'}
  call plug#end()		
let g:coc_global_extensions = ['coc-emmet', 'coc-prettier', 'coc-sh', 'coc-go', 'coc-clangd', 'coc-tsserver']

set list lcs=tab:\|\ 
"StatusLine
set statusline+=%#Title#\ \ \ [\ %{ObsessionStatus()}\ ]\ \ \ 
set statusline+=%#Directory#\ \ \ %#Directory#\ \ \ [\ %f\ ]\ \ \ 
set statusline+=%=
set statusline+=%#QuickFixLine#\ \ \ %y\ \ \ 
set statusline+=%#QuickFixLine#\ \ \ [\ %l,%c\ ]\ \ \ 
set statusline+=%#QuickFixLine#\ \ \ [\ %p%%\ ]\ \ \ 
set statusline+=%m\ \ \ 

"set statusline=%<%f\ %=%-14.(%l,%c%V%)\ %P
"set list
"set listchars=tab:\|·\|,trail:…,extends:…,precedes:…,space:·   

set shiftwidth=2
set mouse=a
set noswapfile 
set nu 
set relativenumber
au BufWinEnter * syntax on

"ColorScheme
"set termguicolors
"let g:gruvbox_italic = 1
"let g:gruvbox_transparent_bg = 1
"let g:gruvbox_italicize_strings = 1
"let g:gruvbox_contrast_dark = "hard"
"colorscheme gruvbox

"set termguicolors     " enable true colors support
"let ayucolor="light"  " for light version of theme
"let ayucolor="mirage" " for mirage version of theme
"let ayucolor="dark"   " for dark version of theme
let transparent_background = "true"
colorscheme catppuccin 

"Copy
vmap <C-c> "+y

"Functions 
function! QuickSave()
	:w!
endfunction
nnoremap <c-c> :call QuickSave()<CR>

function! CleanExit()
	:q!
endfunction
nnoremap <c-z> :call CleanExit()<CR>

function! SpawnTerminal()
  set splitbelow
  split term://bash
  resize 8
endfunction
nnoremap <c-n> :call SpawnTerminal()<CR>


"Fzf Search
nnoremap <C-p> :FZF<CR>
let g:fzf_action = { 'ctrl-t': 'tab split', 'ctrl-s': 'split', 'ctrl-v': 'vsplit'}
let g:fzf_preview_window = ['up:40%:hidden', 'ctrl-/']


"" use alt+hjkl to move between split/vsplit panels
tnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-l> <C-\><C-n><C-w>l
nnoremap <A-left> <C-w>h
nnoremap <A-down> <C-w>j
nnoremap <A-up> <C-w>k
nnoremap <A-right> <C-w>l
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>


"Prettier 
command! -nargs=0 Prettier :CocCommand prettier.formatFile

"Coc

inoremap <silent><expr> <c-space> coc#refresh()
inoremap <silent><expr> <Tab>
\ pumvisible() ? "\<C-n>" :
\ <SID>check_back_space() ? "\<Tab>" :
\ coc#refresh()
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"


